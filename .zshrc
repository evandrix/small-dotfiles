function() {

    function source_private() {
        local TWILIO_ZSHRC='twilio/.zshrc.twilio'
        if [ -f $TWILIO_ZSHRC ]; then
            source $TWILIO_ZSHRC
        fi

        local PRIVATE_ZSHRC='.zshrc.private'
        if [ -f $PRIVATE_ZSHRC ]; then
            source $PRIVATE_ZSHRC
        fi
    }

    function set_path() {

        PATHDIRS=(
            $HOME/bin
            $HOME/twilio/bin

            # Virtualenv dependencies
            $HOME/code/httpie/venv/bin
            #$HOME/code/twilio-jsonapi/venv/bin

            # Golang bin directories
            $HOME/code/go/bin
            /usr/local/go/bin
            /usr/local/heroku/bin

            $HOME/code/google_appengine
            /Applications/Postgres.app/Contents/MacOS/bin
            /Applications/Postgres93.app/Contents/MacOS/bin
            /usr/local/mysql/bin
            /opt/local/bin
            /opt/local/sbin
            /usr/local/sbin
            /usr/local/Cellar/python32/3.2.3/bin
            /usr/local/Cellar/ruby/1.9.2-p290/bin
            /usr/local/Cellar/ruby/2.0.0-p195/bin
            /usr/local/Cellar/ruby/2.0.0-p247/bin
            /usr/local/Cellar/ruby/2.0.0-p353/bin
            /usr/local/share/npm/bin
            /usr/local/Cellar/node/0.10.12/bin

            /usr/local/gcc-4.8.0-qt-4.8.4-for-mingw32/win32-gcc/bin
            /usr/local/mingw-w32-bin_i686-darwin/bin
        )

        for dir in $PATHDIRS; do
            if [ -d $dir ]; then
                path=($path $dir)
            fi
        done

        # add whichever go version we have
        if [[ -d /usr/local/Cellar/go ]]; then
            for dir in $(ls -d /usr/local/Cellar/go/*); do
                if [[ -d "$dir/libexec/bin" ]]; then
                    path=($path "$dir/libexec/bin")
                fi
            done
        fi

        # append some paths to beginning to pick up correct python, pyenv, rvm.
        path=($HOME/code/pyenv/bin $HOME/code/rbenv/bin /usr/local/opt/pyenv/shims /usr/local/bin $path)

        local uname=$(uname)
        export GOPATH=$HOME/code/go:$HOME/code/matasano:$HOME/code/snapchat-friends/server:/usr/local/go:$GOPATH
        if [[ "$uname" == "Linux" ]]; then
            export GOPATH=/usr/local/go:$GOPATH
        fi

        openrct2dir=/usr/local/cross-tools/i686-w64-mingw32/lib/pkgconfig
        if [[ -d "$openrct2dir" ]]; then
            export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:$openrct2dir
        fi
    }

    function oh_my_zsh() {

        # Path to your oh-my-zsh configuration.
        local ZSH=$HOME/.oh-my-zsh

        # Comment this out to disable weekly auto-update checks
        # DISABLE_AUTO_UPDATE="true"

        setopt list_ambiguous
        setopt menu_complete
        setopt dvorak

        COMPLETION_WAITING_DOTS="true"

        # zsh settings (0.005s)
        plugins=(fab git lein)
        source $ZSH/oh-my-zsh.sh
        autoload -U add-zsh-hook
    }

    # Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
    # Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
    # Example format: plugins=(rails git textmate ruby lighthouse)

    function set_lazy_load_fns() {

        # autojump
        # runtime is 0.009s (mainly shelling out to homebrew)
        function j() {
            (( $+commands[brew] )) && { 
                local pfx=$(brew --prefix)
                [[ -f "$pfx/etc/autojump.sh" ]] && . "$pfx/etc/autojump.sh"
                j "$@"
            }
            [[ -s /root/.autojump/etc/profile.d/autojump.sh ]] && source /root/.autojump/etc/profile.d/autojump.sh
        }

        pyenv() {
            eval "$( command pyenv init - )"
            pyenv "$@"
        }

        rbenv() {
            eval "$( command rbenv init - )"
            rbenv "$@"
        }

        # added by travis gem
        # runtime is ~0.009, commented out
        # [[ -f /Users/kevin/.travis/travis.sh ]] && source /Users/kevin/.travis/travis.sh

        [[ -f /nix/var/nix/profiles/default/etc/profile.d/nix.sh ]] && source /nix/var/nix/profiles/default/etc/profile.d/nix.sh || true

        # XXX lazy load these
        # virtualenv settings
        local wrapper_location="/usr/local/bin/virtualenvwrapper.sh"
        if [ -f "$wrapper_location" ]; then
            source "$wrapper_location"
        fi

        # anywhere from 0.005-0.008s
        if echo $SHELL | grep bash; then 
            local git_completion="/etc/bash_completion.d/git"
            if [[ -f $git_completion ]]; then
                #source $git_completion
            fi
        fi

    }

    function prompt_char {
        git branch >/dev/null 2>/dev/null && echo '±' && return
        hg root >/dev/null 2>/dev/null && echo '☿' && return
    }

    function get_time {
        local seconds=${1}
        local minutes=0
        local hours=0
        while [ $seconds -gt 59 ]; do
            ((minutes = minutes + 1))
            ((seconds = seconds - 60))
        done
        while [ $minutes -gt 59 ]; do
            ((hours = hours + 1))
            ((minutes = minutes - 60))
        done
        echo "$hours $minutes $seconds"
    }

    function precmd {
        DIR=$(pwd|sed -e "s!$HOME!~!");
        if [ ${#DIR} -gt 60 ]; then 
            cur="${DIR:0:27}...${DIR:${#DIR}-30}";
        else
            cur=${DIR}
        fi;
        set -A _elapsed $_elapsed $(( SECONDS-_start ))
        result="`get_time $_elapsed[-1]`"
        hours=$(echo $result | cut -f1 -d" ")
        minutes=$(echo $result | cut -f2 -d" ")
        seconds_int=$(echo $result | cut -f3 -d" ")
        if [ "$hours" -eq "0" ]; then
            if [ "$minutes" -eq "0" ]; then
                _time="$seconds_int"s
            else
                seconds=`printf "%02d" "$seconds_int"`
                _time="$minutes":"$seconds"
            fi
        else
            seconds=`printf "%02d" "$seconds_int"`
            minutes=`printf "%02d" "$minutes"`
            _time="$hours:$minutes:$seconds"
        fi
    }

    # see http://stackoverflow.com/a/2704689/329700
    preexec () {
       (( $#_elapsed > 1000 )) && set -A _elapsed $_elapsed[-1000,-1]
       typeset -ig _start=SECONDS
    }

    function print_color () { 
        echo -ne "%{\e[38;05;${1}m%}"; 
    }


    function set_prompt() {

        # color guide: http://misc.flogisoft.com/_media/bash/colors_format/256-colors.sh.png
        # Sadly can't make these local since they are used at every prompt.
        RED=`print_color 160`
        ORANGE=`print_color 172`
        BLUE=`print_color 79`
        BRIGHTBLUE=`print_color 33`
        HOSTBLUE=`print_color 45`
        YELLOW=`print_color 226`
        GREEN=`print_color 82`
        PURPLE=`print_color 170`
        PROMPT_COLOR=`print_color 30`
        if [ "$HOME" = '/home/kevin' -o "$HOME" = '/home/kevinburke' ]; then
            alias ls='ls --color';
            PROMPT_COLOR=`print_color 196`
        fi

        local SCREENSET=""
        if [[ "$TERM" == "screen" ]];
        then
            SCREENSET="[screen]\n"
        fi

        DEFAULT="%{$fg[default]%}"
        ZSH_THEME_GIT_PROMPT_PREFIX="$GREEN("
        ZSH_THEME_GIT_PROMPT_SUFFIX=")"

        function git_status {
            inf=$(git_prompt_info);
            if [[ -n "$inf" ]]; then
                GITST=$(git_prompt_info)" "
            else
                GITST=''
            fi
        }

        add-zsh-hook precmd git_status


        if [[ "$USERNAME" == "root" ]]; then
            user_color=$RED;
        else
            user_color=$ORANGE;
        fi

        if [[ "`hostname`" == "rhino" ]]; then
            host_color="$HOSTBLUE";
        else
            host_color="$PURPLE";
        fi

        PROMPT='
$BLUE${_time} $SCREENSET$user_color%n${DEFAULT} at ${host_color}%M${DEFAULT} in $YELLOW${cur}
$PURPLE`prompt_char` $GREEN$GITST%(?.$BRIGHTBLUE.$RED)$ ${DEFAULT}';

    }

    function set_aliases() {
        alias curl='noglob curl'
        alias http='noglob http'
        alias make='nocorrect make'
        alias go='nocorrect go'
        alias jsonapi='noglob jsonapi'

        alias inotebook='pushd ~/.ipython_notebooks; workon ipython && ipython notebook; deactivate; popd'

        # Unix command aliases
        alias ll='ls -lah'
        alias l=ll
        alias lll=ll
        alias less='less -M'
        alias vi=vim
        alias amke='make'
        alias svi='sudo vim'
        alias mv='mv -i'
        alias cp='cp -iv'
        alias pcp='rsync --partial --progress -ah'
        alias df='df -h'
        alias grep='grep -i --color=auto'
        alias jx='javac \!$.java ; java \!$'
        alias viewmemory="ps -u $USER -o rss,command | grep -v peruser | awk '{sum+=\$1} END {print sum/1024}'"
        alias viewprocesses="ps -u $USER -o rss,etime,pid,command"

        alias mostusedcomms="history | awk '{CMD[\$2]++;count++;}END { for (a in CMD)print CMD[a] \" \" CMD[a]/count*100 \"% \" a;}' | grep -v \"./\" | column -c3 -s \" \" -t | sort -nr | nl |  head -n20"

        alias vp='pushd ~/code/chef/vagrants/cluster; vagrant provision; big done; popd'
        alias vssh='pushd ~/code/chef/vagrants/cluster; vagrant ssh; popd'
        alias vup='pushd ~/code/chef/vagrants/cluster; vagrant up; big done; popd'
        alias youtube='youtube-dl --add-metadata -t --extract-audio -f 18'

        alias c='cd'
        alias sd='say -vzarvox "done"'

        # these save so much time
        alias ..="cd .."
        alias ..2="cd ../.."
        alias ..3="cd ../../.."
        alias ..4="cd ../../../.."
        alias ..5="cd ../../../../.."
        alias ..6="cd ../../../../../.."
        alias ..7="cd ../../../../../../.."


        # linux
        (( $+commands[gvim] )) && {
            alias vi=gvim;
            alias svi='sudo gvim'
        }

        # set up macvim, if it exists
        (( $+commands[mvim] )) && { 
            alias vi=mvim; 
            alias svi='sudo mvim'
        }

        # ask for conf if deleting more than three files; otherwise, delete w/o conf
        # use rm -I if it exists
        (( $+commands[grm] )) && {
            alias rm='grm -I';
        }
    }

    function set_git_aliases() {

        alias gclone='cd ~/code && git clone'
        alias gitst='git status'

        push_branch() {
            branch=`git rev-parse --symbolic-full-name --abbrev-ref HEAD`
            git push $1 $branch
        }
        pull_branch() {
            branch=`git rev-parse --symbolic-full-name --abbrev-ref HEAD`
            git pull $1 $branch
        }
        autoload push_branch
        autoload pull_branch

        alias gpo='git push origin'
        alias gpom='git push origin master'
        alias gpob='push_branch origin'

        alias gfo='git fetch origin'
        alias gfu='git fetch upstream'
        alias gfot='git fetch origin --tags'
        alias gfut='git fetch upstream --tags'
        alias gpot='git push origin --tags'
        alias gput='git push upstream --tags'

        alias gpu='git push upstream'
        alias gpum='git push upstream master'
        alias gpub='push_branch upstream'

        alias glo='git pull origin'
        alias glom='git pull origin master'
        alias glob='pull_branch origin'
        alias glum='git pull upstream master'
        alias glub='pull_branch upstream'

        alias gadd='git add'
    }


    function print_fortune() {
        # total runtime of this script is 0.04s
        echo
        # https://github.com/kevinburke/weirdfortune
        weirdfortune --all
    }

    ######### end function definitions #############

    PROFILE_STARTUP=false

    if [[ "$PROFILE_STARTUP" == true ]]; then
        # http://zsh.sourceforge.net/Doc/Release/Prompt-Expansion.html
        PS4=$'%D{%M%S%.} %N:%i> '
        exec 3>&2 2>$HOME/tmp/startlog.$$
        setopt xtrace prompt_subst
    fi

    source_private
    set_path
    oh_my_zsh
    set_aliases
    set_git_aliases
    set_lazy_load_fns
    set_prompt
    print_fortune

    # via http://stackoverflow.com/a/4351664/329700
    if [ "$PROFILE_STARTUP" = true ]; then
        unsetopt xtrace
        exec 2>&3 3>&-
    fi

}
