#!/bin/bash

set -ev

main() {
    if vim -s "check-command-t.vs"; then
        echo "command-t installed! congratulations. quitting"
        clear
        exit 0
    fi
    clear

    PATH="$HOME/.pyenv/shims:/usr/bin:$PATH"
    local HOMEBREW_RUBY=false
    if [[ `brew list | grep ruby` ]]; then
        HOMEBREW_RUBY=true
    fi
    if [[ "$HOMEBREW_RUBY" == true ]]; then
        brew unlink ruby
        brew uninstall vim || true
    fi

    git submodule add git://git.wincent.com/command-t.git $HOME/.vim/bundle/command-t || true
    git submodule init || true

    pushd $HOME/.vim/bundle/command-t/ruby/command-t
        if [[ -f Makefile ]]; then
            make clean
        fi
        /usr/bin/ruby extconf.rb
        make
    popd

    if [[ $HOMEBREW_RUBY == true ]]; then
        # We can't use /usr/local/bin for the path since /usr/local/bin/ruby is
        # the troublemaker. We can't set pyenv shell because that uses the
        # shell function which is unavailable.
        pyenv local 2.7.8 || true
        brew link ruby
        brew install vim --debug --verbose
        pyenv shell pypy-2.4.0 || true
    fi
}
main
