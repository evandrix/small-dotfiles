execute pathogen#infect()
syntax on

filetype plugin indent on
set nocompatible

set encoding=utf-8
set modelines=0
highlight MatchParen ctermbg=4
set laststatus=2
"set statusline=%F%m%r%h%w%=(%{&ff}/%Y)\ (line\ %l\/%L,\ col\ %c)
set statusline=%<%f\ (%{&ft})\ %-4(%m%)%=%-19(%3l,%02c%03V%)
set noerrorbells
set cursorline
set t_vb=
set t_Co=256
set history=1000
set scrolloff=8
set sidescrolloff=5
set autoindent
set showmode
set splitright
set showcmd
set hidden
set wildmenu
set wildmode=list:longest
set wildignore+=*.hi,*.pyc,*.class,*.o,*.dSYM,*.dSYM/,*.egg-info,*.egg-info/,venv,build,cover,_build,include
set visualbell
set ttyfast
set ruler
set backspace=indent,eol,start
set laststatus=2
set lazyredraw
set t_ti= t_te=

" automatically change directory to current file dir
"set autochdir

"" default tab settings - should override these for specific extensions (HTML,
"" etc)
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
set wrap
set textwidth=79
" c = auto wrap comments on 79 line length
set formatoptions=cqrn1

"" search settings
nnoremap / /\v
vnoremap / /\v
set ignorecase
set smartcase
set gdefault
set incsearch
set showmatch
set hlsearch

if version >= 703
    set colorcolumn=80
    hi ColorColumn ctermbg=NONE guibg=#cccccc
    "set undofile
endif

"" gather all .swp, .un~ in same directory
if version >= 703
    set undodir=~/.vim/tmp/undo/
    set backupdir=~/.vim/tmp/backup/
    set directory=~/.vim/tmp/swap/
endif

if has("signs") && version >= 703
    set relativenumber
endif

"" show line endings
set list
set listchars=tab:+\ ,eol:¬
set formatprg=par\ -w79\ -T4

"" mappings
nnoremap <up> <nop>
nnoremap <down> <nop>
nnoremap <left> <nop>
nnoremap <right> <nop>
inoremap <up> <nop>
inoremap <down> <nop>
inoremap <left> <nop>
inoremap <right> <nop>
nnoremap j gj
nnoremap k gk
map <C-k> <C-w><Up>
map <C-j> <C-w><Down>
map <C-l> <C-w><Right>
map <C-h> <C-w><Left>
inoremap <F1> <ESC>
nnoremap <F1> <ESC>
vnoremap <F1> <ESC>
inoremap jj <ESC>

nnoremap Q @='n.'<CR>

"" one less key for write
nnoremap ; :

"" matchit remapping
nmap <tab> %
vmap <tab> %

"" end of line
nnoremap L $
vnoremap L $

"" center after jump
nnoremap n nzz
nnoremap N Nzz

"" for Gundo plugin
let mapleader = ","
"if has("python")
    "nnoremap <Leader>u :GundoToggle<CR>
"end

"" clear a search
nnoremap <leader><space> :nohlsearch<cr>

nnoremap <leader>c gg"+yG<cr>

" flush commandt results
nnoremap <leader>f :CommandTFlush<cr>

nnoremap <leader>g :GoFmt<cr>

nnoremap <leader>r :call RunFile()<CR>
nnoremap <leader>n :call RunTestsInFile()<CR>
nnoremap <leader>p :!/usr/local/twilio/src/php/tests/nphpunit --all
nnoremap <leader>i :!/usr/local/twilio/src/php/tests/nphpunit --integration

function! ReloadChrome()
    execute 'silent !osascript ' . 
                \'-e "tell application \"Google Chrome\" " ' .
                \'-e "repeat with i from 1 to (count every window)" ' .
                \'-e "tell active tab of window i" ' . 
                \'-e "reload" ' .
                \'-e "end tell" ' .
                \'-e "end repeat" ' .
                \'-e "end tell" >/dev/null'
endfunction

function! RunFile()
    if(&ft=='python')
        exec ":!source venv/bin/activate; python " . bufname('%')
    endif
endfunction

function! RunTestsInFile()
    if(&ft=='php')
        if(match(getcwd(), "php-helper") != 0)
            :execute "!" . "/usr/local/bin/phpunit --configuration /Users/kevin/code/php-helper/tests/phpunit.xml " . bufname('%') . ' \| grep -v Configuration \| egrep -v "^$" '
        else
            :execute "!" . "/usr/local/bin/phpunit -d memory_limit=512M -c /usr/local/twilio/src/php/tests/config.xml --bootstrap /usr/local/twilio/src/php/tests/bootstrap.php " . bufname('%') . ' \| grep -v Configuration \| egrep -v "^$" '
        endif
    elseif(&ft=='go')
        exec ":!go test ./..."
    elseif(&ft=='javascript')
        exec ":!jasmine-node " . bufname('%')
    elseif(&ft=='python')
        exec ":!" . ". venv/bin/activate; nosetests " . bufname('%')  . " --stop"
    endif
endfunction

function! RunTest()
    if (&ft=='python')
        execute "normal" ?def test_<CR>wyw
    endif
endfunction

nnoremap <leader>o :call ReloadChrome()<CR>:pwd<cr>

"" visually select last edited text
nmap gV `[v`]

let g:solarized_termcolors=256
set t_Co=256
set background=dark
colorscheme jellybeans

"" autosave on lose focus
"au FocusLost * :wa

""to avoid crontab edit errors"
set backupskip=/tmp/*,/private/tmp/*,*.zip


if has('cmdlog')
  " cmdlogdir:
  "     The directory which will be used to store logs.
  set cmdlogdir=~/.vimlogs/
  "" cmdloginsert:
  ""     Log text entered in normal mode.
  ""     Disabled by default
  set cmdloginsert
end

if has("autocmd")

    "" delete trailing whitespace when you save a file
    autocmd BufWritePre *.php,*.py,*.js,*.txt,*.hs,*.java,*.md :call <SID>StripTrailingWhitespaces()

    " auto wrap .txt files to 80 characters
    autocmd BufRead,BufNewFile *.txt set formatoptions+=t

    " twilio uses tabs.
    autocmd BufRead,BufNewFile /usr/local/twilio/src/*,*.go,*.c set noexpandtab
    autocmd BufRead,BufNewFile ~/code/java-helper set noexpandtab
    autocmd BufRead,BufNewFile *.go.jinja set noexpandtab

    " except for python.
    autocmd BufRead,BufNewFile *.py,*.md,*.scss,*.conf,*.conf.disabled set expandtab

    autocmd BufRead,BufNewFile *.php.mock set ft=php

    autocmd BufRead,BufNewFile *.mustache set ft=html syntax=html
    autocmd BufRead,BufNewFile *.jinja set ft=jinja
    autocmd BufRead,BufNewFile *.jinja :syntax on

    "" for my compilers class
    autocmd BufRead,BufNewFile *.bcs set filetype=cs

    autocmd BufRead,BufNewFile *.go set filetype=go

    " detect text filetype
    autocmd BufEnter * if &filetype == "" | setlocal ft=txt | endif

    autocmd FileType haml,html,ruby setlocal shiftwidth=2 tabstop=2 softtabstop=2

    autocmd FileType javascript set autoread

    autocmd FileType html set filetype=htmldjango
    autocmd FileType mustache set filetype=htmldjango
    autocmd FileType mako set filetype=mako
    autocmd FileType modula2 set filetype=markdown

    "if exists(':RainbowParenthesesToggle') == 2   
        "au bufenter * exe RainbowParenthesesToggle 
    "endif
    "if exists(':RainbowParenthesesLoadRound') == 2 
        "au Syntax * exe RainbowParenthesesLoadRound
    "endif
    "if exists(':RainbowParenthesesLoadSquare') == 2 
        "au Syntax * exe RainbowParenthesesLoadSquare
    "endif
    "if exists(':RainbowParenthesesLoadBraces') == 2 
        "au Syntax * exe RainbowParenthesesLoadBraces
    "endif

    let g:rbpt_colorpairs = [
                \ ['red', 'LightSkyBlue1'],
                \ ['lightblue', 'MediumPurple3'],
                \ ['brown', 'Gold3'],
                \ ['Darkblue', 'DeepSkyBlue4'],
                \ ]

    "" close vim if only file open is a nerdtree
    autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif
    let NERDTreeShowHidden=1

    let g:startify_show_dir = 0

    let g:gist_post_private = 1
endif

let g:go_fmt_command = "goimports"

function! <SID>StripTrailingWhitespaces()
    " save last search & cursor position
    let _s=@/
    let l = line(".")
    let c = col(".")
    %s/\s\+$//e
    let @/=_s
    call cursor(l, c)
endfunction

if has("gui_running")
    set guioptions=egmrt
endif

"" haskell settings
"let g:haddock_browser = "/Applications/Google Chrome.app"

"" for screen
"if match($TERM, "screen")!=-1
    "set term=xterm
"endif

"" look for a tags file, starting in current dir and working up to root
set tags=tags;/

"" CoffeeScript settings
"" let coffee_compile_on_save = 1

let CommandTMaxFiles = 40000

set rtp+=/Users/kevin/code/go/misc/vim

"" set bash
""set shell=bash\ --login

"" set syntastic file checking
"let g:syntastic_enable_signs=1
""let g:syntastic_stl_format = '[%E{Err: %fe #%e}%B{, }%W{Warn: %fw #%w}]'
"let g:syntastic_stl_format = '[Syntax: line:%F (%t)]'
"" this should display errors in the statusline.
"set statusline+=%{SyntasticStatuslineFlag()}
"run SyntasticEnable javascript
"run SyntasticEnable coffee
"run SyntasticEnable html
"run SyntasticEnable python
"run SyntasticEnable haskell
"run SyntasticEnable haml

