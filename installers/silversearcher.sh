#!/bin/bash

set -ev

# May need to install xz, pcre-devel
if grep -qi centos /etc/issue; then
    yum -y install xz-devel pcre-devel automake
fi

pushd ~/code
    if [ ! -f ~/bin/ag ]; then 
        if [ ! -d the_silver_searcher ]; then
            git clone git@github.com:ggreer/the_silver_searcher.git
        fi
        pushd the_silver_searcher
            ./build.sh --prefix $HOME
            # May need to run this as root.
            make install
        popd
    fi
popd
