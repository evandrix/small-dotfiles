#!/bin/bash

set -ev

VERSION='1.2.1'
BINARY="go$VERSION.src.tar.gz"
ROOT=/usr/local/go

pushd ~/src
    if [ ! -f "$BINARY" ]; then
        curl --location -O "https://go.googlecode.com/files/$BINARY"
    fi
    if [ ! -d "$DIRECTORY" ]; then
        tar -xf "$BINARY"
    fi
    pushd go/src
        if [ ! -f /usr/local/go/bin/go ]; then
            mkdir -p $ROOT
            GOROOT_FINAL=$ROOT ./make.bash
            cp ~/src/go/* /usr/local/go
        fi
    popd
popd


# Vim settings
mkdir -p $HOME/.vim/ftdetect
mkdir -p $HOME/.vim/syntax
mkdir -p $HOME/.vim/autoload/go
if [ ! -L $HOME/.vim/ftdetect/gofiletype.vim ]; then
    ln -s $ROOT/misc/vim/ftdetect/gofiletype.vim $HOME/.vim/ftdetect/
fi
if [ ! -L $HOME/.vim/syntax/go.vim ]; then
    ln -s $ROOT/misc/vim/syntax/go.vim $HOME/.vim/syntax
fi
if [ ! -L $HOME/.vim/autoload/go/complete.vim ]; then
    ln -s $ROOT/misc/vim/autoload/go/complete.vim $HOME/.vim/autoload/go
fi
