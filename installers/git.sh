#!/bin/bash

set -ev

VERSION='1.9.0'
DIRECTORY="git-$VERSION"
BINARY="$DIRECTORY.tar.gz"

if grep -qi centos /etc/issue; then
    yum -y install zlib-devel perl-ExtUtils-MakeMaker gettext curl-devel
fi
pushd ~/src
    if [ ! -f "$BINARY" ]; then 
        curl --location -O "https://git-core.googlecode.com/files/$BINARY"
    fi
    if [ ! -d "$DIRECTORY" ]; then
        tar -xf "$BINARY"
    fi
    pushd "$DIRECTORY"
        ./configure --prefix $HOME
        make && make install
        if [ $? -gt 0 ]; then
            exit 2
        fi
    popd
popd
